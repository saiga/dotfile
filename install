#!/usr/bin/env bash
DOTFILES_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

shopt -s dotglob
shopt -s extglob
shopt -s globstar
shopt -s nullglob

# Link to .config/$folder.
for folder in bash bspwm dunst feh git htop picom polybar nvim i3 sxhkd; do
  mkdir -pv ~/.config/$folder/
  cp -r "$folder" ~/.config/
done

# Link gtk.
mkdir -pv ~/.config/{gtk-2.0,gtk-3.0}
cp "$DOTFILES_DIR"/gtk/gtkrc-2.0 ~/.config/gtk-2.0/gtkrc
cp "$DOTFILES_DIR"/gtk/settings.ini ~/.config/gtk-3.0/

# Create data directories.
mkdir -pv ~/.local/share/{bash,less}/

# Initialize database file for mpd.
touch ~/.local/share/mpd/database

# Create screenshot folder for mpv.
mkdir -pv ~/Pictures/Screenshots/

# Join array with delimiter.
join() {
  local IFS="$1"
  shift
  echo "$*"
}

log_file="$DOTFILES_DIR/install.log"
printf '' > "$log_file"

declare -a programs=( acpi blueman bspwm i3 i3blocks i3status i3lock imagemagick jq network-manager numix-gtk-theme papirus-icon-theme rofi scrot slop sysstat xbacklight xclip xinput xsetroot dunst feh mkfontscale xset htop picom font-awesome-ttf polybar atool cabextract highlight imagemagick p7zip w3m rtorrent rxvt-unicode xrdb curl llvm-clang pandoc pip ripgrep )
programs_string=$(join " " "${programs[@]}")
installed_program_list=$(eopkg list-installed --install-info)

sudo eopkg install --yes-all --component system.devel
sudo eopkg install --yes-all $programs_string
# Checking if applications are installed.
for f in "${programs[@]}"; do
  if [ "$(echo "$installed_program_list" | grep --count "$f ")" -eq 1 ]; then
    echo "$f successfully installed." >>"$log_file"
  else
    echo "$f failed to install." >>"$log_file"
  fi
done

# Installing powerline fonts.
sudo rm -f /usr/share/fonts/conf.d/70-no-bitmaps.conf
sudo ln -svf /usr/share/fontconfig/conf.avail/70-yes-bitmaps.conf /usr/share/fonts/conf.d
mkdir -pv ~/.local/share/fonts/ && cp -u ./fonts/*.{ttf,bdf} ~/.local/share/fonts/ && (cd ~/.local/share/fonts/ && mkfontdir && mkfontscale)
fc-cache --force --verbose

# Apply ~/.Xresources.
xrdb ~/.Xresources
chmod +x ~/.config/bspwm/bspwmrc

